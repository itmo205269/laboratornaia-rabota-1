﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Notebook
    {
            static void Main(string[] args)
            {
                int id = 0; ;  string Surname = ""; string Name = ""; string middleName = ""; string PhoneNumber = ""; string Country = "";  string Birthday = ""; string Company = ""; string Position = ""; string Othernotes = "";
                RecordNote note = new RecordNote();
            Console.WriteLine("Записная книжка готова к работе. Для создания полной новой записи введите: in");
            Console.WriteLine("Для создания упрощенной записи введите: inmin");
            Console.WriteLine("Для удаления записи введите: del");
            Console.WriteLine("Для поиска уже созданной записи введите: find");
            Console.WriteLine("Для отображения всех созданных записей введите: sort");
            Console.WriteLine("Для отображения в упрощенном виде введите: print");
            Console.WriteLine("Для выхода из программы введите: exit");
                Console.WriteLine("Введите комманду");
                do
                {
                    switch (Console.ReadLine())
                    {
                        case "in": note.AddRecord(Surname, Name, middleName, PhoneNumber, Country, Birthday, Company, Position, Othernotes); break;
                        case "inmin": note.AddRecordmin(Surname, Name, PhoneNumber, Country); break;
                        case "del": note.DeleteRecords(Surname); break;
                        case "find": note.printlist(note.SearchRecords(Surname)); break;
                        case "sort": note.SortBySurname(); break;
                        case "print": note.pritfun(); break;
                        case "exit": Note.brake = true; break;
                        default: Console.WriteLine("Не верная команда, попробуйте еще раз"); break;
                    }
                } while (!Note.brake);


            }        
    }
}
