﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class Note
    {
        public static int id = 0;
        internal string Surname;
        internal string Name;
        internal string middleName;
        internal string PhoneNumber;
        internal string Country;
        internal string Birthday;
        internal string Company;
        internal string Position;
        internal string Othernotes;
        internal static bool brake = false;

        public Note( string Surname, string Name, string middleName, string PhoneNumber, string Country, string Birthday, string Company, string Position, string Othernotes)
        {
            int ID = id++;
            this.Surname = Surname;
            this.Name = Name;
            this.middleName = middleName;
            this.PhoneNumber = PhoneNumber;
            this.Country = Country;
            this.Birthday = Birthday;
            this.Company = Company;
            this.Position = Position;
            this.Othernotes = Othernotes;
        }
        public Note(string Surname, string Name, string PhoneNumber, string Country)
        {
            int ID = id++;
            this.Surname = Surname;
            this.Name = Name;
            this.PhoneNumber = PhoneNumber;
            this.Country = Country;
        }
    }
}
