﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class RecordNote
    {

        List<Note> notebookRecords;
        List<Note> NotebookRecords
        {
            get { return notebookRecords; }
            set { notebookRecords = value; }
        }

        public RecordNote()
        {
            notebookRecords = new List<Note>();
        }

        public List<Note> SearchRecords(string Surname)
        {
            Console.Write("введите Фамилию \n");
            Surname = Convert.ToString(Console.ReadLine());
            List<Note> ret = new List<Note>();
            foreach (Note rec in notebookRecords)
                if (rec.Surname == Surname)                    
                    ret.Add(rec);            
            return ret;
        }

        
        public void printlist(List<Note> ret)
        {

            foreach (Note t in ret)
            {
                Console.WriteLine("id= {0},surname= {1}, name= {2}, middleName= {3}, phone= {4}, country= {5}, birthday= {6}, company= {7}, position= {8}, othernotes= {9}", Note.id, t.Surname, t.Name, t.middleName, t.PhoneNumber, t.Country, t.Birthday, t.Company, t.Position, t.Othernotes);
            }
        }
        public void AddRecord(string Surname, string Name, string middleName, string PhoneNumber, string Country, string Birthday, string Company, string Position, string Othernotes)
        {
            Note.id++;
            Console.Write("Введите Фамилию \n");
            Surname = Convert.ToString(Console.ReadLine());
            Console.Write("Введите Имя \n");
            Name = Convert.ToString(Console.ReadLine());
            Console.Write("Введите Отчество \n");
            middleName = Convert.ToString(Console.ReadLine());
            Console.Write("Введите номер телефона \n");
            PhoneNumber = Convert.ToString(Console.ReadLine());
            Console.Write("Введите Страну \n");
            Country = Convert.ToString(Console.ReadLine());
            Console.Write("Введите дату рождения \n");
            Birthday = Convert.ToString(Console.ReadLine());
            Console.Write("Введите название Компании \n");
            Company = Convert.ToString(Console.ReadLine());
            Console.Write("Введите должность \n");
            Position = Convert.ToString(Console.ReadLine());
            Console.Write("Введите прочие заметки \n");
            Othernotes = Convert.ToString(Console.ReadLine());
            Note rec = new Note(Surname, Name, middleName, PhoneNumber, Country, Birthday, Company, Position, Othernotes);
            notebookRecords.Add(rec);
            Console.WriteLine("id= {0},surname= {1}, name= {2}, middleName= {3}, phone= {4}, country= {5}, birthday= {6}, company= {7}, position= {8}, othernotes= {9}", Note.id, Surname, Name, middleName, PhoneNumber, Country, Birthday, Company, Position, Othernotes);
            Console.WriteLine("Запись создана");
        }

        public void pritfun()
        {

            foreach (Note c in notebookRecords)
            {
                Console.WriteLine("surname= {0}, name= {1}, phone= {2}", c.Surname, c.Name, c.PhoneNumber);
            }
        }

        public void DeleteRecords(string Surname)
        {
            List<Note> records = SearchRecords(Surname);
            foreach (Note rec in records)
                notebookRecords.Remove(rec);
            Console.WriteLine("Запись удалена");
        }

        public void SortBySurname()
        {
            IEnumerable<Note> r = notebookRecords.OrderBy(name => name.Surname);
            //notebookRecords.Reverse();
            foreach (Note c in r)
            {
                Console.WriteLine("surname= {0}, name= {1}, middleName= {2}, phone= {3}, country= {4}, birthday= {5}, company= {6}, position= {7}, othernotes= {8}", c.Surname, c.Name, c.middleName, c.PhoneNumber, c.Country, c.Birthday, c.Company, c.Position, c.Othernotes);
            }
        }
        public void AddRecordmin(string Surname, string Name, string PhoneNumber, string Country)
        {
            Note.id++;
            Console.Write("Введите Фамилию \n");
            Surname = Convert.ToString(Console.ReadLine());
            Console.Write("Введите Имя \n");
            Name = Convert.ToString(Console.ReadLine());
            Console.Write("Введите номер телефона \n");
            PhoneNumber = Convert.ToString(Console.ReadLine());
            Note rec = new Note(Surname, Name, PhoneNumber, Country);
            notebookRecords.Add(rec);
            Console.Write("Введите Страну \n");
            Country = Convert.ToString(Console.ReadLine());
            Console.WriteLine("id= {0},surname= {1}, name= {2}, phone= {3}, country= {4}", Note.id, Surname, Name, PhoneNumber,Country);
            Console.WriteLine("Запись создана");
        }

    }   

}

